﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    Rigidbody rb;
    private int count;
    public float speed;
    public Text countText;
    // Start is called before the first frame update
    void Start()
    {
        //Added count variable to later give as input to ui
        rb = GetComponent<Rigidbody>();
        //Set it to 0 at first to represent the amount of boxes collected
        count = 0;
        SetCountText();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float moveHorizonal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizonal, 0f, moveVertical);

        //Speed is set to 10
        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            //Gives input for the ui to display the amount of boxes collected so far 
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
    }

    void SetCountText ()
    {
        countText.text = "Count: " + count.ToString();
    }
}
